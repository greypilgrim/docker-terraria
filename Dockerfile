FROM debian:buster-slim

MAINTAINER wroot

RUN apt-get update \
    && apt-get install -q -y ca-certificates

COPY config/debian-buster-sources.list /etc/apt/sources.list

RUN apt-get update
RUN apt-get -y install wget screen unzip curl

RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /root/.cache

ENV DATA_DIR="/serverdata"
ENV SERVER_DIR="${DATA_DIR}/serverfiles"
ENV GAME_VERSION="template"
ENV GAME_MOD="template"
ENV GAME_PARAMS="template"
ENV UMASK=027
ENV UID=1000
ENV GID=1000

RUN mkdir $DATA_DIR
RUN mkdir $SERVER_DIR
RUN groupadd --gid 1000 terraria
RUN useradd -d $DATA_DIR -s /bin/bash --uid $UID --gid $GID terraria
RUN chown -R terraria $DATA_DIR
RUN chown -R terraria $SERVER_DIR

RUN ulimit -n 2048

ADD /scripts/ /opt/scripts/
RUN chmod -R 770 /opt/scripts/
RUN chown -R terraria /opt/scripts

USER terraria

#Server Start
ENTRYPOINT ["/opt/scripts/start-server.sh"]
