# Terraria Dedicated Server in Docker

This Docker will download and install Terraria and run it.

***SERVER PASSWORD: Docker***

To upgrade: Change the game version to whatever version you want and restart the docker.

## Env params
| Name | Value | Example |
| --- | --- | --- |
| SERVER_DIR | Folder for gamefiles | /serverdata/serverfiles |
| GAME_PARAMS | Commandline startup parameters | -config serverconfig.txt |
| GAME_VERSION | Preferred Game version | 1.3.5.3 |

## Run example
```
docker run --name Terraria -d \
-p 7777:7777/udp \
--env 'GAME_PARAMS=-config serverconfig.txt' \
--env 'GAME_VERSION=1.3.5.3' \
-v /mnt/user/appdata/terraria:/serverdata/serverfiles \
--restart=unless-stopped \
ich777/terrariaserver:latest # TODO
```
